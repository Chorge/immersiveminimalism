﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCell {

    public List<GameCell> m_allNeigbours;
    public List<GameCell> m_sideNeigbours;

    public GameCell m_LeftNeigbour;
    public GameCell m_RightNeigbour;
    public GameCell m_UpNeigbour;
    public GameCell m_DownNeigbour;

    //bool m_isAlive = false;
    //public bool m_aliveNextRound = false;

    int m_faction = 0; // 0 == dead 1 == alive 
    int m_nextRoundFaction = 0; // 0 == dead 1 == alive  

    public Vector2Int m_positionOnGrid;

    public int m_age;

    public int m_transitionTreshold;
    public int m_transitionSteps;
     
    Color m_myColor;

    public GameCell()
    {
        m_allNeigbours = new List<GameCell>();
        m_sideNeigbours = new List<GameCell>(); 
    }

    public Color GetColor()
    {
        return m_myColor;
    } 

    public void SetAlive(bool alive, bool freshBorn)
	{
        if (freshBorn)
        {
            m_age = GetValidSelection().m_ageUnits;
            //m_isAlive = alive;
            if (alive) { m_faction = 1; } else { m_faction = 0; }
        }
        //else if (m_isAlive && alive)        { m_age++; m_age = Mathf.Min(m_age, GetValidSelection().m_ageUnits); }
        //else if (!m_isAlive && !alive) { m_age++; m_age = Mathf.Min(m_age, GetValidSelection().m_ageUnits); }
        else if (!(m_faction == 0) && alive)        { m_age++; m_age = Mathf.Min(m_age, GetValidSelection().m_ageUnits); }
        else if ((m_faction == 0) && !alive) { m_age++; m_age = Mathf.Min(m_age, GetValidSelection().m_ageUnits); }        
        else
        {  
            m_age = 0;
            //m_isAlive = alive;
            if (alive) { m_faction = 1; } else { m_faction = 0; }
        }

        if (alive)
        {  
            m_myColor = ChangeCellColorAlive(); 
        }
        else
        {
             m_myColor = ChangeCellColorDead();
        }
    }


    public void SetFaction(int faction, bool freshBorn)
    {
        if (freshBorn)
        {
            m_age = GetValidSelection().m_ageUnits;
            //m_isAlive = alive;
            m_faction = faction;
        }
        //else if (m_isAlive && alive)        { m_age++; m_age = Mathf.Min(m_age, GetValidSelection().m_ageUnits); }
        //else if (!m_isAlive && !alive) { m_age++; m_age = Mathf.Min(m_age, GetValidSelection().m_ageUnits); }
        else if (m_faction == faction) { m_age++; m_age = Mathf.Min(m_age, GetValidSelection().m_ageUnits); }
        else
        {
            m_age = 0;
            //m_isAlive = alive; 
            m_faction = faction;
        }

        switch (faction)
        {
            case (0):
                {
                    m_myColor = ChangeCellColorDead();
                    break;
                }
            case (1):
                {
                    m_myColor = ChangeCellColorAlive();
                    break;
                }
            case (2):
                {
                    m_myColor = ChangeCellColorFaction3();
                    break;
                }
            case (3):
                {
                    m_myColor = ChangeCellColorFaction4();
                    break;
                }
        }
    }
    public bool IsAlive()
	{
        return !(m_faction == 0);
		//return m_isAlive;
	}

    public int GetFaction() { return m_faction; }
    public int GetNextRoundFaction() { return m_nextRoundFaction; }

    public void SetFactionNextRound(int faction) { m_nextRoundFaction = faction; }
     

	public void AddNeigbour_All(GameCell cell, bool addAlsoToNeigbour)
	{
		if (cell != null) {
  
			//if (!m_allNeigbours.Contains (cell)) {
                m_allNeigbours.Add (cell);
			//}

			if (addAlsoToNeigbour) {
				cell.AddNeigbour_All(this,false);
			}
		}
	}

    public void AddNeigbour_Left(GameCell cell, bool addAlsoToNeigbour)
    {
        if (cell != null)
        {
            m_LeftNeigbour = cell;

            if (addAlsoToNeigbour)
            {
                cell.AddNeigbour_Right(this, false);
            }
        }
    }

    public void AddNeigbour_Right(GameCell cell, bool addAlsoToNeigbour)
    {
        if (cell != null)
        {
            m_RightNeigbour = cell;

            if (addAlsoToNeigbour)
            {
                cell.AddNeigbour_Left(this, false);
            }
        }
    }

    public void AddNeigbour_Down(GameCell cell, bool addAlsoToNeigbour)
    {
        if (cell != null)
        {
            m_DownNeigbour = cell;

            if (addAlsoToNeigbour)
            {
                cell.AddNeigbour_Up(this, false);
            }
        }
    }

    public void AddNeigbour_Up(GameCell cell, bool addAlsoToNeigbour)
    {
        if (cell != null)
        {
            m_UpNeigbour = cell;

            if (addAlsoToNeigbour)
            {
                cell.AddNeigbour_Down(this, false);
            }
        }
    }

    public void AddNeigbour_Side(GameCell cell, bool addAlsoToNeigbour)
    {
        if (cell != null)
        {

            //if (!m_sideNeigbours.Contains(cell))
            //{
                m_sideNeigbours.Add(cell);
            //}

            if (addAlsoToNeigbour)
            {
                cell.AddNeigbour_Side(this, false);
            }
        }
    }

    public bool LeftNeighbourIsAlive()
    {
        return (m_LeftNeigbour != null && m_LeftNeigbour.IsAlive());
    }

    public bool RightNeighbourIsAlive()
    {
        return (m_RightNeigbour != null && m_RightNeigbour.IsAlive());
    }

    /*public static void ResetGrowingColors()
    {
        m_dyingColor = m_inbetweenColor;
        m_bornColor  = m_inbetweenColor; 
    }*/

    /*public static void UpdateGrowingColors()
    {
        if (m_dyingColor != m_deadColor)
        {
            m_dyingColor = Color.Lerp(m_dyingColor, m_deadColor,Time.deltaTime / 2 );
        }

        if (m_bornColor != m_aliveColor)
        {
            m_bornColor = Color.Lerp(m_bornColor, m_aliveColor, Time.deltaTime / 2 );
        } 
    }*/

    Color ChangeCellColorAlive()
    {
        if (GetValidSelection().m_cellcolor == CellColor.AgeAlive || GetValidSelection().m_cellcolor == CellColor.Both)
        {
            if (m_age == 0) return GetValidColor_1age(); 
            else if (m_age >= GetValidSelection().m_ageUnits) { return GetValidColor_1(); }

            return Color.Lerp(GetValidColor_1age(),
                              GetValidColor_1(),
                              (float)m_age / (float)GetValidSelection().m_ageUnits);
        }

        return GetValidColor_1();
    }

    Color ChangeCellColorDead()
    {
        if (GetValidSelection().m_cellcolor == CellColor.AgeDead || GetValidSelection().    m_cellcolor == CellColor.Both)
        {
            if (m_age == 0) { return GetValidColor_2age(); }
            else if(m_age >= GetValidSelection().m_ageUnits) { return GetValidColor_2(); }

            return Color.Lerp(GetValidColor_2age(),
                              GetValidColor_2(),
                              (float)m_age / (float)GetValidSelection().m_ageUnits);
        }

        return GetValidColor_2();
    }

    Color ChangeCellColorFaction3()
    {
        if (GetValidSelection().m_cellcolor == CellColor.AgeDead || GetValidSelection().m_cellcolor == CellColor.Both)
        {
            if (m_age == 0) { return GetValidColor_2age(); }
            else if (m_age >= GetValidSelection().m_ageUnits) { return GetValidColor_3(); }

            return Color.Lerp( GetValidColor_2age(),
                               GetValidColor_3(),
                              (float)m_age / (float)GetValidSelection().m_ageUnits);
        }

        return GetValidColor_3();
    }

    Color ChangeCellColorFaction4()
    {
        if (GetValidSelection().m_cellcolor == CellColor.AgeAlive || GetValidSelection().m_cellcolor == CellColor.Both)
        {
            if (m_age == 0) return GetValidColor_1age();
            else if (m_age >= GetValidSelection().m_ageUnits) { return GetValidColor_4(); }

            return Color.Lerp( GetValidColor_1age(),
                               GetValidColor_4(),
                              (float)m_age / (float)GetValidSelection().m_ageUnits);
        }

        return GetValidColor_4();
    }


    SelectionSection GetValidSelection()
    {
        if (SelectionManager.m_lastSelection == null) return SelectionManager.m_currentSelection;

        if (m_transitionTreshold > m_transitionSteps)
        {
            return SelectionManager.m_lastSelection;
        }
        else
        {
            return SelectionManager.m_currentSelection;
        }
    } 

    Color GetValidColor_1()
    {
        if (SelectionManager.m_lastSelection == null) return SelectionManager.m_currentSelection.m_color_1;

        if (m_transitionSteps == 0) return SelectionManager.m_lastSelection.m_color_1;

        return Color.Lerp(SelectionManager.m_lastSelection.m_color_1,
                          SelectionManager.m_currentSelection.m_color_1,
                          Mathf.Clamp( (float)m_transitionSteps / (float)SelectionManager.m_lastSelection.m_caBlendingUnits, 0f, 1f));  

    }

    Color GetValidColor_2()
    {
        if (SelectionManager.m_lastSelection == null) return SelectionManager.m_currentSelection.m_color_2;

        if (m_transitionSteps == 0) return SelectionManager.m_lastSelection.m_color_2;


        return Color.Lerp(SelectionManager.m_lastSelection.m_color_2,
                          SelectionManager.m_currentSelection.m_color_2,
                          Mathf.Clamp((float)m_transitionSteps / (float)SelectionManager.m_lastSelection.m_caBlendingUnits, 0f, 1f));

    }

    Color GetValidColor_3()
    {
        if (SelectionManager.m_lastSelection == null) return SelectionManager.m_currentSelection.m_color_3;

        if (m_transitionSteps == 0) return SelectionManager.m_lastSelection.m_color_3;
         
        return Color.Lerp(SelectionManager.m_lastSelection.m_color_3,
                          SelectionManager.m_currentSelection.m_color_2,
                          Mathf.Clamp((float)m_transitionSteps / (float)SelectionManager.m_lastSelection.m_caBlendingUnits, 0f, 1f));

    }

    Color GetValidColor_4()
    {
        if (SelectionManager.m_lastSelection == null) return SelectionManager.m_currentSelection.m_color_4;

        if (m_transitionSteps == 0) return SelectionManager.m_lastSelection.m_color_4; 

        return Color.Lerp(SelectionManager.m_lastSelection.m_color_4,
                          SelectionManager.m_currentSelection.m_color_1,
                          Mathf.Clamp((float)m_transitionSteps / (float)SelectionManager.m_lastSelection.m_caBlendingUnits, 0f, 1f));

    }

    Color GetValidColor_1age()
    {
        if (SelectionManager.m_lastSelection == null) return SelectionManager.m_currentSelection.m_age_color_1;

        if (m_transitionSteps == 0) return SelectionManager.m_lastSelection.m_age_color_1; 

        return Color.Lerp(SelectionManager.m_lastSelection.m_age_color_1,
                          SelectionManager.m_currentSelection.m_age_color_1,
                          Mathf.Clamp((float)m_transitionSteps / (float)SelectionManager.m_lastSelection.m_caBlendingUnits, 0f, 1f));

    }

    Color GetValidColor_2age()
    {
        if (SelectionManager.m_lastSelection == null) return SelectionManager.m_currentSelection.m_age_color_2;

        if (m_transitionSteps == 0) return SelectionManager.m_lastSelection.m_age_color_2; 

        return Color.Lerp(SelectionManager.m_lastSelection.m_age_color_2,
                          SelectionManager.m_currentSelection.m_age_color_2,
                          Mathf.Clamp((float)m_transitionSteps / (float)SelectionManager.m_lastSelection.m_caBlendingUnits, 0f, 1f));

    }

    public bool UsesLastSelection()
    {
        if (SelectionManager.m_lastSelection == null) return false;

        if (m_transitionTreshold > m_transitionSteps)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsAliveNextRound()
    {
        return m_nextRoundFaction != 0;
    }

    public void SetAliveNextRound(bool set)
    {
        if (set == true) { m_nextRoundFaction = 1; } else { m_nextRoundFaction = 0; }
    }
}
