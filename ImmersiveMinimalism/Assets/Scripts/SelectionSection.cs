﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
 
public class SelectionSection : MonoBehaviour {

    public CellularAutomata m_ca = CellularAutomata.GameOfLife;
    public StartSetup m_start = StartSetup.Full;
    public CellColor m_cellcolor = CellColor.None;

    public bool m_useNoise = false;
    public float m_noiseAmount = 0.5f;

    public Color m_color_1 = new Color(0.239f, 0.294f, 0.427f);
    public Color m_color_2 = new Color(0.629f, 0.696f, 0.872f);

    public Color m_color_3 = new Color(1.0f, 0.0f, 0.0f);
    public Color m_color_4 = new Color(0.0f, 0.0f, 1.0f);

    public Color m_age_color_1 = new Color(1f, 1f, 1f);
    public Color m_age_color_2 = new Color(0f, 0f, 0f);

    public int m_transitionUnits = 100;
    public int m_ageUnits = 50;

    public int m_caBlendingUnits = 50;


    [SerializeField] Dropdown m_ca_dropdown;
    [SerializeField] Dropdown m_start_dropdown;
    [SerializeField] Dropdown m_noise_dropdown;
    [SerializeField] Dropdown m_cellcolor_dropdown;


    [SerializeField] Image m_color1hex;
    [SerializeField] Image m_color2hex;
    [SerializeField] Image m_color1ageHex;
    [SerializeField] Image m_color2ageHex; 

    [SerializeField] Image m_color3hex;
    [SerializeField] Image m_color4hex;

    /*[SerializeField] Image m_color1r;
    [SerializeField] Image m_color1g;
    [SerializeField] Image m_color1b;
    [SerializeField] Image m_color2r;
    [SerializeField] Image m_color2g;
    [SerializeField] Image m_color2b;

    [SerializeField] Image m_age_color1r;
    [SerializeField] Image m_age_color1g;
    [SerializeField] Image m_age_color1b;
    [SerializeField] Image m_age_color2r;
    [SerializeField] Image m_age_color2g;
    [SerializeField] Image m_age_color2b;*/

    public void Start()
    {
        m_ca_dropdown.value = (int)m_ca;
        m_start_dropdown.value = (int)m_start;
        if (m_useNoise == true)
        {
            if (m_noiseAmount <= 0.25f) { m_noise_dropdown.value = 1; }
            else
            if (m_noiseAmount <= 0.5f) { m_noise_dropdown.value = 2; }
            else
            { m_noise_dropdown.value = 3; }
        }
        else { m_noise_dropdown.value = 0; }
        m_cellcolor_dropdown.value = (int)m_cellcolor;
        
        /*m_color1r.color = m_color_1;
        m_color1g.color = m_color_1;
        m_color1b.color = m_color_1;
        
        m_color2r.color = m_color_2;
        m_color2g.color = m_color_2;
        m_color2b.color = m_color_2;

        m_color1r.color = m_color_1;
        m_color1g.color = m_color_1;
        m_color1b.color = m_color_1;

        m_color2r.color = m_color_2;
        m_color2g.color = m_color_2;
        m_color2b.color = m_color_2;

        m_age_color1r.color = m_age_color_1;
        m_age_color1g.color = m_age_color_1;
        m_age_color1b.color = m_age_color_1;  

        m_age_color2r.color = m_age_color_2;
        m_age_color2g.color = m_age_color_2;
        m_age_color2b.color = m_age_color_2;*/

        //DontDestroyOnLoad(this.gameObject);
    }

    public void Trigger_CA_Option(Dropdown myOption)
    {
        m_ca = (CellularAutomata)myOption.value;
    }

    public void Trigger_Start_Option(Dropdown myOption)
    {
        m_start = (StartSetup)myOption.value;
    }

    public void Trigger_Noise_Option(Dropdown myOption)
    {
        m_useNoise = myOption.value != 0;

        if (myOption.value == 1) { m_noiseAmount = 0.25f; }
        if (myOption.value == 2) { m_noiseAmount = 0.5f; }
        if (myOption.value == 3) { m_noiseAmount = 0.75f; }

    }

    public void Trigger_CellColor_Option(Dropdown myOption)
    {
        m_cellcolor = (CellColor)myOption.value;
    }

    /*public void Trigger_Color1R_Change(InputField input)
    {
        m_color_1.r = float.Parse(input.text);
        m_color1r.color = m_color_1;
        m_color1g.color = m_color_1;
        m_color1b.color = m_color_1;
    }*/

    public void Trigger_TransitionUnits(InputField myInput)
    {
        m_transitionUnits = int.Parse( myInput.text );
    }

    public void Trigger_AgeUnits(InputField myInput)
    {
        m_ageUnits = int.Parse(myInput.text);
    }

    public void Trigger_CaBlendingUnits(InputField myInput)
    {
        m_caBlendingUnits = int.Parse(myInput.text);
    }

    /*public void Trigger_Color1G_Change(InputField input)
    {
        m_color_1.g = float.Parse(input.text);
        m_color1r.color = m_color_1;
        m_color1g.color = m_color_1;
        m_color1b.color = m_color_1;
    }


    public void Trigger_Color1B_Change(InputField input)
    {
        m_color_1.b = float.Parse(input.text);
        m_color1r.color = m_color_1;
        m_color1g.color = m_color_1;
        m_color1b.color = m_color_1;
    }

    public void Trigger_Color2R_Change(InputField input)
    {
        m_color_2.r = float.Parse(input.text);
        m_color2r.color = m_color_2;
        m_color2g.color = m_color_2;
        m_color2b.color = m_color_2;
    }

    public void Trigger_Color2G_Change(InputField input)
    {
        m_color_2.g = float.Parse(input.text);
        m_color2r.color = m_color_2;
        m_color2g.color = m_color_2;
        m_color2b.color = m_color_2;
    }

    public void Trigger_Color2B_Change(InputField input)
    {
        m_color_2.b = float.Parse(input.text);
        m_color2r.color = m_color_2;
        m_color2g.color = m_color_2;
        m_color2b.color = m_color_2;
    }

    public void Trigger_AgeColor1R_Change(InputField input)
    {
        m_age_color_1.r = float.Parse(input.text);
        m_age_color1r.color = m_age_color_1;
        m_age_color1g.color = m_age_color_1;
        m_age_color1b.color = m_age_color_1;
    }

    public void Trigger_AgeColor1G_Change(InputField input)
    {
        m_age_color_1.g = float.Parse(input.text);
        m_age_color1r.color = m_age_color_1;
        m_age_color1g.color = m_age_color_1;
        m_age_color1b.color = m_age_color_1;
    }

    public void Trigger_AgeColor1B_Change(InputField input)
    {
        m_age_color_1.b = float.Parse(input.text);
        m_age_color1r.color = m_age_color_1;
        m_age_color1g.color = m_age_color_1;
        m_age_color1b.color = m_age_color_1;
    }

    public void Trigger_AgeColor2R_Change(InputField input)
    {
        m_age_color_2.r = float.Parse(input.text);
        m_age_color2r.color = m_age_color_2;
        m_age_color2g.color = m_age_color_2;
        m_age_color2b.color = m_age_color_2;
    }

    public void Trigger_AgeColor2G_Change(InputField input)
    {
        m_age_color_2.g = float.Parse(input.text);
        m_age_color2r.color = m_age_color_2;
        m_age_color2g.color = m_age_color_2;
        m_age_color2b.color = m_age_color_2;
    }

    public void Trigger_AgeColor2B_Change(InputField input)
    {
        m_age_color_2.b = float.Parse(input.text);
        m_age_color2r.color = m_age_color_2;
        m_age_color2g.color = m_age_color_2;
        m_age_color2b.color = m_age_color_2;
    }*/ 

    public void Trigger_Color1HEX_Change(InputField input)
    {
        Color newCol;

        if (ColorUtility.TryParseHtmlString(input.text, out newCol))
        {
            m_color_1 = newCol;
            m_color1hex.color = m_color_1;

        }
    }

    public void Trigger_Color2HEX_Change(InputField input)
    {
        Color newCol;

        if (ColorUtility.TryParseHtmlString(input.text, out newCol))
        {
            m_color_2 = newCol;
            m_color2hex.color = m_color_2;
        }
    }

    public void Trigger_Color1AgeHEX_Change(InputField input)
    {
        Color newCol;

        if (ColorUtility.TryParseHtmlString(input.text, out newCol))
        {
            m_age_color_1 = newCol;
            m_color1ageHex.color = m_age_color_1;

        }
    }

    public void Trigger_Color2AgeHEX_Change(InputField input)
    {
        Color newCol;

        if (ColorUtility.TryParseHtmlString(input.text, out newCol))
        {
            m_age_color_2 = newCol;
            m_color2ageHex.color = m_age_color_2;

        }
    }

    public void Trigger_Color3HEX_Change(InputField input)
    {
        Color newCol;

        if (ColorUtility.TryParseHtmlString(input.text, out newCol))
        {
            m_color_3 = newCol;
            m_color3hex.color = m_color_1; 
        }
    }

    public void Trigger_Color4HEX_Change(InputField input)
    {
        Color newCol;

        if (ColorUtility.TryParseHtmlString(input.text, out newCol))
        {
            m_color_4 = newCol;
            m_color4hex.color = m_color_1;
        }
    }
}
