﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CA_VoterModel
{

    static public void Calculate(GameCell cell)
    {
        bool flipeCoin = Random.value < 05f;
        cell.SetAliveNextRound(cell.IsAlive());
        if (flipeCoin)
        {
            int randomNeigbour = Random.Range(-1, cell.m_allNeigbours.Count);

            if (randomNeigbour != -1)
            {
               cell.SetAliveNextRound( cell.m_allNeigbours[randomNeigbour].IsAlive() );
            }
        }
    }

    static public void Set(GameCell cell)
    {
        cell.SetAlive(cell.IsAliveNextRound(), false);
    }

}
