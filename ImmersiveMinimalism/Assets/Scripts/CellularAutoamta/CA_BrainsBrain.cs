﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CA_BrainsBrain
{
    static public void Calculate(GameCell cell)
    {
        int aliveNeigbours = 0; 

        foreach (GameCell neighbour in cell.m_allNeigbours)
        { 
            if (neighbour.GetFaction() == 1)
            {
                aliveNeigbours++;
            }
        }

        if (cell.GetFaction() == 2)
        {
            cell.SetFactionNextRound(0);
        }
        else if (cell.GetFaction() == 1)
        {
            cell.SetFactionNextRound(0);
        }
        else if (cell.GetFaction() == 0)
        {
            if (aliveNeigbours == 2)
            {
                cell.SetFactionNextRound(1);
            }
        } 
    }

    static public void Set(GameCell cell)
    {
        cell.SetFaction(cell.GetNextRoundFaction(), false);
    }

}
