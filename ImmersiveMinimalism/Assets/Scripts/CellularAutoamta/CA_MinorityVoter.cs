﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CA_MinorityVoter
{

    static public void Calculate(GameCell cell)
    { 
        cell.SetAliveNextRound(cell.IsAlive());

        int aliveNeigbours = 0; 

        foreach (GameCell neighbour in cell.m_allNeigbours)
        { 
            if (neighbour.IsAlive())
            {
                aliveNeigbours++;
            }
        }

        int randomNeigbour = Random.Range(-1, cell.m_allNeigbours.Count);
         
        if (randomNeigbour != -1)
        {
            bool randomAlive = cell.m_allNeigbours[randomNeigbour].IsAlive();

            if (randomAlive && aliveNeigbours >= 4)
            {
                cell.SetAliveNextRound(false);
            }

            if (!randomAlive && aliveNeigbours < 4)
            {
                cell.SetAliveNextRound(true);
            }
        }
    }

    static public void Set(GameCell cell)
    {
        cell.SetAlive(cell.IsAliveNextRound(), false);
    }

}
