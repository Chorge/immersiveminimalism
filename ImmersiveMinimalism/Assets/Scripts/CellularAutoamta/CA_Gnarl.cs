﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CA_Gnarl
{
    static public void Calculate(GameCell cell)
    {
        int aliveNeigbours = 0;

        bool wasAlive = cell.IsAlive();

        foreach (GameCell neighbour in cell.m_allNeigbours)
        {

            if (neighbour.IsAlive())
            {
                aliveNeigbours++;
            }
        }

        cell.SetAliveNextRound(aliveNeigbours == 1); 
    }

    static public void Set(GameCell cell)
    {
        cell.SetAlive(cell.IsAliveNextRound(), false);
    }

}
