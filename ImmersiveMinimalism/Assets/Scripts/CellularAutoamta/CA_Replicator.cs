﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CA_Replicator
{
    static public void Calculate(GameCell cell)
    {
        int aliveNeigbours = 0;

        bool wasAlive = cell.IsAlive();

        foreach (GameCell neighbour in cell.m_allNeigbours)
        {

            if (neighbour.IsAlive())
            {
                aliveNeigbours++;
            }
        }

        if (cell.IsAlive())
        {

            cell.SetAliveNextRound( aliveNeigbours == 1 || aliveNeigbours == 3 || aliveNeigbours == 5 || aliveNeigbours == 7);
        }
        else
        {
            cell.SetAliveNextRound( aliveNeigbours == 1 || aliveNeigbours == 3 || aliveNeigbours == 5 || aliveNeigbours == 7);
        }
    }

    static public void Set(GameCell cell)
    {
        cell.SetAlive(cell.IsAliveNextRound(), false);
    }
}
