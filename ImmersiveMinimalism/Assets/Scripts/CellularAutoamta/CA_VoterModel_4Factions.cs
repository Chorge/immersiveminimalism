﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CA_VoterModel_4Factions
{

    static public void Calculate(GameCell cell)
    {
        bool flipeCoin = Random.value < 05f;
        cell.SetFactionNextRound(cell.GetFaction());
        if (flipeCoin)
        {
            int randomNeigbour = Random.Range(-1, cell.m_allNeigbours.Count);

            if (randomNeigbour != -1)
            {
               cell.SetFactionNextRound( cell.m_allNeigbours[randomNeigbour].GetFaction() );
            }
        }
    }

    static public void Set(GameCell cell)
    {
        cell.SetFaction(cell.GetNextRoundFaction(), false);
    }

}
