﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum CellularAutomata
{
    GameOfLife,
    VoterModel, 
    Seeds,

    Replicator,
    Inkspots,
    Diamoeba,
    _2x2,
    Highlife,
    DayAndNight,
    Anneal,
    VoterModel_4Factions, 
    Amoeba,
    MajorityVoter,
    MinorityVoter,

    LifeWithoutDead,
    Gnarl,
    Maze,
    //CorrosionOfComformity,
    WalledCity,
    Assimilation,



}

public enum StartSetup
{
    Full,
    MiddleSquare,
    HalfHalf,
    CenterPixel,
    CornerSquares,
    Chessboard, 
    SmallRectangle,
    Tiling,
    SinglePoint,
    MicroSquare,
    MicroFrame,
    MicroStar,
    MicroCross, 

    Border,
    SomeSquares,
    MiddleBar,

    Top,
    Sides,
    BottomBox,
    CenterBox,

    Circle,

    InverseCircle,
    InverseSquare,
    BorderCircle,
    BorderSquare,

    SmallChessboard,
    //SomeCircles,


}

public enum CellColor {

    None,
    AgeAlive,
    AgeDead,
    Both

}

public class SelectionManager : MonoBehaviour {

    [SerializeField] public SelectionSection [] m_selections;

    static public SelectionSection m_lastSelection; 
    static public SelectionSection m_currentSelection;
    static public SelectionSection[] m_allSelections;

    static public int m_currentSelectionIndex = 0;

    static public bool m_captureImages = false;
     

    private void Start()
    {
        m_currentSelection = m_selections[0];
        m_allSelections = m_selections;
        m_currentSelectionIndex = 0;
        m_lastSelection = null; 
    }
     

    public static void DoTransition()
    {
        m_currentSelectionIndex++;

        if (m_currentSelectionIndex < m_allSelections.Length)
        {
            m_lastSelection = m_currentSelection;
            m_currentSelection = m_allSelections[m_currentSelectionIndex];
            Debug.Log("DO TRANSITION - to " + m_currentSelection);

            GameGrid.s_instance.SetTransitionMarkers(m_lastSelection.m_caBlendingUnits);
        } 


    }

    public void Trigger_Capture_Option(Dropdown myOption)
    {
        m_captureImages = myOption.value != 0;  
    }

    

}
