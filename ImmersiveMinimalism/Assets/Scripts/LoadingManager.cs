﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadingManager : MonoBehaviour {

      
    public void LoadScene (string sceneName) {

        SceneManager.LoadScene(sceneName,LoadSceneMode.Single);

        PictureManager.s_picCount = 0; 

    }

    public void UnloadScene(string sceneName)
    { 
            SceneManager.UnloadSceneAsync(sceneName); 
    } 

    public void AddScene(string sceneName)
    { 
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive); 
    }

    public void ActiavteMainCanvas(bool set)
    {
        GameObject canvasObject = GameObject.Find("MainCanvas");

        canvasObject.GetComponent<Canvas>().enabled = set;

        if(set)
        {
            SelectionManager.m_currentSelection = SelectionManager.m_allSelections[0];
            SelectionManager.m_currentSelectionIndex = 0;
            SelectionManager.m_lastSelection = null;
        }
    }


    public void Set8K_Resolution()
    {
        Screen.SetResolution(7680, 4320, FullScreenMode.ExclusiveFullScreen);
    }
}
