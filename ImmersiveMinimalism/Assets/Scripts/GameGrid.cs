﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CA_Dimension
{
    _1D,
    _2D,
}


public class GameGrid : MonoBehaviour
{

    [SerializeField] Vector2Int m_gridsize = new Vector2Int(512, 512);

    List<GameCell> m_allCells = new List<GameCell>();

    [SerializeField] float m_simulationTimeStep;

    [SerializeField] Image m_renderImage;

    delegate void CellFunction(GameCell cell);

    Texture2D m_rendertexture;

    float m_simulationTimeStamp;
    bool m_playSimulation;

    CA_Dimension m_dimensions;

    int m_gridSteps = 0;

    int m_noiseFactions = 2;

    CellFunction m_calculateCellularAutomata;
    CellFunction m_setCellularAutomata;

    CellFunction m_previousCalculateCellularAutomata;
    CellFunction m_prevrioisSetCellularAutomata;

    public static GameGrid s_instance;

    // Use this for initialization
    void Start()
    {
        s_instance = this;

        SetupCellularAutomata();

        m_rendertexture = new Texture2D(m_gridsize.x, m_gridsize.y);
 
        m_dimensions = CA_Dimension._2D; 

        Vector3 cameraPostion = Camera.main.transform.position;
        cameraPostion.y = m_gridsize.y + 3;
        Camera.main.transform.position = cameraPostion;

        GameCell cell_upperLeft = null;
        GameCell cell_lowerLeft = null;
        GameCell cell_upper = null;
        GameCell cell_left = null;
        List<GameCell> leftCells = new List<GameCell>();
        List<GameCell> currentCells = new List<GameCell>();

        GameCell gameCell = null;

        int index = 0;

        for (int index_x = 0; index_x < m_gridsize.x; index_x++)
        {
            for (int index_y = 0; index_y < m_gridsize.y; index_y++)
            {
                GameCell newCell = new GameCell();

                if (m_dimensions == CA_Dimension._2D || index_y == 0)
                {
                    switch (SelectionManager.m_currentSelection.m_start)
                    {
                        case (StartSetup.Full):
                            {
                                newCell.SetFaction(NoiseCheck(true), true);
                                break;
                            }

                        case (StartSetup.CornerSquares):
                            {
                                bool isAlive = false;
                                if (index_x < m_gridsize.y / 4 || index_x > m_gridsize.x - m_gridsize.y / 4)
                                {
                                    if (m_dimensions == CA_Dimension._1D ||
                                        index_y < m_gridsize.y / 4 ||
                                        index_y > m_gridsize.y - m_gridsize.y / 4)

                                    {
                                        isAlive = true;
                                    }
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.MiddleSquare):
                            {
                                bool isAlive = false;

                                if (index_x < m_gridsize.x / 2 + m_gridsize.y / 4
                                    && index_x > m_gridsize.x / 2 - m_gridsize.y / 4)
                                {
                                    if (index_y < m_gridsize.y / 2 + m_gridsize.y / 4
                                        && index_y > m_gridsize.y / 2 - m_gridsize.y / 4) 
                                    {
                                        isAlive = true;
                                    }
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.HalfHalf):
                            {
                                newCell.SetFaction(NoiseCheck(index_x < m_gridsize.x / 2), true);
                                break;
                            }
                        case (StartSetup.CenterPixel):
                            {
                                newCell.SetFaction(NoiseCheck(index_x == m_gridsize.x / 2), true);
                                break;
                            }

                        case (StartSetup.Chessboard):
                            {
                                bool isAlive = false;

                                if (index_x % (m_gridsize.y / 20) < m_gridsize.y / 40)
                                {
                                    if (m_dimensions == CA_Dimension._1D || index_y % (m_gridsize.y / 20) < m_gridsize.y / 40)
                                    {
                                        isAlive = true;
                                    }
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.SmallRectangle):
                            {
                                bool isAlive = false;

                                if (index_x == m_gridsize.y / 4 || index_x == m_gridsize.x - m_gridsize.y / 4 ||
                                    index_y == m_gridsize.y / 4 || index_y == m_gridsize.y - m_gridsize.y / 4)
                                {
                                    isAlive = true;
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);
                                break;
                            }

                        case (StartSetup.Tiling):
                            {
                                if (index_x % 2 == 1)
                                {
                                    newCell.SetFaction(NoiseCheck(index_y % 2 == 0), true);
                                }
                                else
                                {
                                    newCell.SetFaction(NoiseCheck(index_y % 2 == 1), true);
                                }
                                break;
                            }

                        case (StartSetup.SinglePoint):
                            {
                                newCell.SetFaction(NoiseCheck(index_x == m_gridsize.x / 2 && index_y == m_gridsize.y / 2), true);
                                break;
                            }

                        case (StartSetup.MicroSquare):
                            {
                                if ((index_x == m_gridsize.x / 2 && index_y == m_gridsize.y / 2) ||
                                    (index_x == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y == m_gridsize.y / 2))
                                {
                                    newCell.SetFaction(NoiseCheck(true), true);
                                }
                                else
                                {
                                    newCell.SetAlive(false, true);
                                }
                                break;
                            }

                        case (StartSetup.MicroFrame):
                            {
                                if ((index_x + 1 == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x - 1 == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y == m_gridsize.y / 2) ||
                                    (index_x - 1 == m_gridsize.x / 2 && index_y == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y - 1 == m_gridsize.y / 2) ||
                                    (index_x == m_gridsize.x / 2 && index_y - 1 == m_gridsize.y / 2) ||
                                    (index_x - 1 == m_gridsize.x / 2 && index_y - 1 == m_gridsize.y / 2))
                                {
                                    newCell.SetFaction(NoiseCheck(true), true);
                                }
                                else
                                {
                                    newCell.SetAlive(false, true);
                                }
                                break;
                            }


                        case (StartSetup.MicroStar):
                            {
                                if ((index_x - 1 == m_gridsize.x / 2 && index_y - 1 == m_gridsize.y / 2) ||
                                    (index_x - 1 == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y - 1 == m_gridsize.y / 2))
                                {
                                    newCell.SetFaction(NoiseCheck(true), true);
                                }
                                else
                                {
                                    newCell.SetAlive(false, true);
                                }
                                break;
                            }

                        case (StartSetup.MicroCross):
                            {
                                if ((index_x - 1 == m_gridsize.x / 2 && index_y - 1 == m_gridsize.y / 2) ||
                                    (index_x - 1 == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x == m_gridsize.x / 2 && index_y == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y + 1 == m_gridsize.y / 2) ||
                                    (index_x + 1 == m_gridsize.x / 2 && index_y - 1 == m_gridsize.y / 2))
                                {
                                    newCell.SetFaction(NoiseCheck(true), true);
                                }
                                else
                                {
                                    newCell.SetAlive(false, true);
                                }
                                break;
                            }

                        case (StartSetup.Border):
                            {
                                bool isAlive = false;

                                if (index_x < m_gridsize.y / 8 || index_x > m_gridsize.x - m_gridsize.y / 8 ||
                                    index_y < m_gridsize.y / 8 || index_y > m_gridsize.y - m_gridsize.y / 8)
                                {
                                    isAlive = true;
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);
                                break;
                            }

                        case (StartSetup.SomeSquares):
                            {
                                bool isAlive = false;

                                if (index_x % (m_gridsize.y / 10) < m_gridsize.y / 40)
                                {
                                    if (m_dimensions == CA_Dimension._1D || index_y % (m_gridsize.y / 10) < m_gridsize.y / 40)
                                    {
                                        isAlive = true;
                                    }
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.MiddleBar):
                            {
                                bool isAlive = false;

                                if (m_dimensions == CA_Dimension._1D ||
                                    (index_y > m_gridsize.y / 2.5 &&
                                    index_y < m_gridsize.y - m_gridsize.y / 2.5))

                                {
                                    isAlive = true;
                                }
                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.Top):
                            {
                                bool isAlive = false;

                                if (index_y > m_gridsize.y - m_gridsize.y / 10)
                                {
                                    isAlive = true;
                                }
                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.Sides):
                            {
                                bool isAlive = false;

                                if (m_dimensions == CA_Dimension._1D ||
                                    (index_x < m_gridsize.y / 11 ||
                                    index_x > m_gridsize.x - m_gridsize.y / 11))

                                {
                                    isAlive = true;
                                }
                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.BottomBox):
                            {
                                bool isAlive = false;

                                if (index_x > m_gridsize.y / 2.1 && index_x < m_gridsize.x - m_gridsize.y / 2.1)
                                {
                                    if (m_dimensions == CA_Dimension._1D ||
                                        index_y < m_gridsize.y / 2.1)
                                    {
                                        isAlive = true;
                                    }
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);
                                break;
                            }

                        case (StartSetup.CenterBox):
                            {
                                bool isAlive = false;

                                if (index_x > m_gridsize.y / 2.01 && index_x < m_gridsize.x - m_gridsize.y / 2.01)
                                {
                                    if (m_dimensions == CA_Dimension._1D ||
                                        (index_y > m_gridsize.y / 2.01 &&
                                        index_y < m_gridsize.y - m_gridsize.y / 2.01))

                                    {
                                        isAlive = true;
                                    }
                                }
                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.Circle):
                            {
                                newCell.SetFaction(NoiseCheck(isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 2, m_gridsize.y / 4, index_x, index_y)), true);

                                break;
                            }

                        case (StartSetup.InverseCircle):
                            {
                                newCell.SetFaction(NoiseCheck(!isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 2, m_gridsize.y / 4, index_x, index_y)), true);

                                break;
                            }

                        case (StartSetup.InverseSquare):
                            {
                                bool isAlive = true;

                                if (index_x < m_gridsize.x / 2 + m_gridsize.y / 4
                                    && index_x > m_gridsize.x / 2 - m_gridsize.y / 4)
                                {
                                    if (index_y < m_gridsize.y / 2 + m_gridsize.y / 4
                                        && index_y > m_gridsize.y / 2 - m_gridsize.y / 4)
                                    {
                                        isAlive = false;
                                    }
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        case (StartSetup.BorderCircle):
                            {
                                bool isAlive = !isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 2, m_gridsize.y / 5, index_x, index_y);

                                if (isAlive)
                                {
                                    isAlive = isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 2, m_gridsize.y / 3, index_x, index_y);
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            } 
                        case (StartSetup.BorderSquare):
                            {
                                bool isAlive = false;

                                if (index_x < m_gridsize.x / 2 + m_gridsize.y / 4
                                    && index_x > m_gridsize.x / 2 - m_gridsize.y / 4)
                                {
                                    if (index_y < m_gridsize.y / 2 + m_gridsize.y / 4
                                        && index_y > m_gridsize.y / 2 - m_gridsize.y / 4)
                                    {
                                        isAlive = true;
                                    }
                                }

                                if (index_x < m_gridsize.x / 2 + m_gridsize.y / 3
                                    && index_x > m_gridsize.x / 2 - m_gridsize.y / 3)
                                {
                                    if (index_y < m_gridsize.y / 2 + m_gridsize.y / 3
                                        && index_y > m_gridsize.y / 2 - m_gridsize.y / 3)
                                    {
                                        isAlive = false;
                                    }
                                } 

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                            case(StartSetup.SmallChessboard):
                            {
                                bool isAlive = false;

                                if (index_x % (m_gridsize.y / 40) < m_gridsize.y / 80)
                                {
                                    if (m_dimensions == CA_Dimension._1D || index_y % (m_gridsize.y / 40) < m_gridsize.y / 80)
                                    {
                                        isAlive = true;
                                    }
                                }

                                newCell.SetFaction(NoiseCheck(isAlive), true);

                                break;
                            }

                        /*case (StartSetup.SomeCircles):
                        {
                                bool isAlive = false;

                                isAlive = isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 2, m_gridsize.y / 40, index_x, index_y);

                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 4, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 4, m_gridsize.y / 4, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 4, m_gridsize.y / 2, m_gridsize.y / 40, index_x, index_y);

                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 3, m_gridsize.y / 3, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 3, m_gridsize.y / 4, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 4, m_gridsize.y / 4, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 4, m_gridsize.y / 3, m_gridsize.y / 40, index_x, index_y);

                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 2, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 2, m_gridsize.y / 3, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 3, m_gridsize.y / 3, m_gridsize.y / 40, index_x, index_y);
                                if (!isAlive)  isAlive = isInsideCircle(m_gridsize.x / 3, m_gridsize.y / 2, m_gridsize.y / 40, index_x, index_y);

                                newCell.SetFaction(NoiseCheck(isAlive), true);  

                                break;
                        }*/
                    }
                }

                if (leftCells.Count != 0) { cell_left = leftCells[index_y]; }

                gameCell = newCell;
                m_allCells.Add(gameCell);

                gameCell.m_positionOnGrid = new Vector2Int(index_x, index_y);

                gameCell.AddNeigbour_All(cell_upperLeft, true);
                gameCell.AddNeigbour_All(cell_lowerLeft, true);
                gameCell.AddNeigbour_All(cell_upper, true);
                gameCell.AddNeigbour_All(cell_left, true);
                gameCell.AddNeigbour_Left(cell_left, true);
                gameCell.AddNeigbour_Side(cell_upper, true);
                gameCell.AddNeigbour_Side(cell_left, true);
                gameCell.AddNeigbour_Up(cell_upper, true);

                cell_upper = gameCell;
                cell_upperLeft = cell_left;

                if (leftCells.Count != 0 && index_y < m_gridsize.y - 2)
                {
                    cell_lowerLeft = leftCells[index_y + 2];
                }

                currentCells.Add(gameCell);
                index++;

            }

            cell_upper = null;
            cell_upperLeft = null;
            cell_lowerLeft = null;

            leftCells.Clear();
            leftCells.InsertRange(0, currentCells);
            currentCells.Clear();
        }

        RenderGridTexture();
    }

    void SetupCellularAutomata()
    {
        m_noiseFactions = 2;

        if (SelectionManager.m_lastSelection != null)
        {
            m_previousCalculateCellularAutomata = m_calculateCellularAutomata;
            m_prevrioisSetCellularAutomata = m_setCellularAutomata; 
        }

        switch (SelectionManager.m_currentSelection.m_ca)
        {
            case (CellularAutomata.GameOfLife):
                {
                    m_calculateCellularAutomata = CA_GameOfLife.Calculate;
                    m_setCellularAutomata = CA_GameOfLife.Set;
                    break;
                }

            case (CellularAutomata.VoterModel):
                {
                    m_calculateCellularAutomata = CA_VoterModel.Calculate;
                    m_setCellularAutomata = CA_VoterModel.Set;
                    break;
                }
                 
            case (CellularAutomata.Seeds):
                {
                    m_calculateCellularAutomata = CA_Seeds.Calculate;
                    m_setCellularAutomata = CA_Seeds.Set;
                    break;
                }

            case (CellularAutomata.Replicator):
                {
                    m_calculateCellularAutomata = CA_Replicator.Calculate;
                    m_setCellularAutomata = CA_Replicator.Set;
                    break;
                }

            case (CellularAutomata.Inkspots):
                {
                    m_calculateCellularAutomata = CA_Inkspots.Calculate;
                    m_setCellularAutomata = CA_Inkspots.Set;
                    break;
                }

            case (CellularAutomata.Highlife):
                {
                    m_calculateCellularAutomata = CA_Highlife.Calculate;
                    m_setCellularAutomata = CA_Highlife.Set;
                    break;
                }

            case (CellularAutomata.Diamoeba):
                {
                    m_calculateCellularAutomata = CA_Diamoeba.Calculate;
                    m_setCellularAutomata = CA_Diamoeba.Set;
                    break;
                }

            case (CellularAutomata.DayAndNight):
                {
                    m_calculateCellularAutomata = CA_DayAndNight.Calculate;
                    m_setCellularAutomata = CA_DayAndNight.Set;
                    break;
                }

            case (CellularAutomata.Anneal):
                {
                    m_calculateCellularAutomata = CA_Anneal.Calculate;
                    m_setCellularAutomata = CA_Anneal.Set;
                    break;
                }

            case (CellularAutomata._2x2):
                {
                    m_calculateCellularAutomata = CA_2x2.Calculate;
                    m_setCellularAutomata = CA_2x2.Set;
                    break;
                }

            case (CellularAutomata.VoterModel_4Factions):
                {
                    m_calculateCellularAutomata = CA_VoterModel_4Factions.Calculate;
                    m_setCellularAutomata = CA_VoterModel_4Factions.Set;
                    m_noiseFactions = 4;
                    break;
                }

            case (CellularAutomata.Amoeba):
                {
                    m_calculateCellularAutomata = CA_Amoeba.Calculate;
                    m_setCellularAutomata = CA_Amoeba.Set; 
                    break;
                }

            case (CellularAutomata.MajorityVoter):
                {
                    m_calculateCellularAutomata = CA_MajorityVoter.Calculate;
                    m_setCellularAutomata = CA_MajorityVoter.Set;
                    break;
                }

            case (CellularAutomata.MinorityVoter):
                {
                    m_calculateCellularAutomata = CA_MinorityVoter.Calculate;
                    m_setCellularAutomata = CA_MinorityVoter.Set;
                    break;
                }

            case (CellularAutomata.LifeWithoutDead):
                {
                    m_calculateCellularAutomata = CA_LifeWithoutDead.Calculate;
                    m_setCellularAutomata = CA_LifeWithoutDead.Set;
                    break;
                }

            case (CellularAutomata.Gnarl):
                {
                    m_calculateCellularAutomata = CA_Gnarl.Calculate;
                    m_setCellularAutomata = CA_Gnarl.Set;
                    break;
                }

            case (CellularAutomata.Maze):
                {
                    m_calculateCellularAutomata = CA_Maze.Calculate;
                    m_setCellularAutomata = CA_Maze.Set;
                    break;
                }

            /*case (CellularAutomata.CorrosionOfComformity):
                {
                    m_calculateCellularAutomata = CA_CorrosionOfComformity.Calculate;
                    m_setCellularAutomata = CA_CorrosionOfComformity.Set;
                    break;
                }*/

            case (CellularAutomata.WalledCity):
                {
                    m_calculateCellularAutomata = CA_WalledCity.Calculate;
                    m_setCellularAutomata = CA_WalledCity.Set;
                    break;
                }

            case (CellularAutomata.Assimilation):
                {
                    m_calculateCellularAutomata = CA_Assimilation.Calculate;
                    m_setCellularAutomata = CA_Assimilation.Set;
                    break;
                }
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (m_playSimulation)
        {

            if (Time.time > m_simulationTimeStamp + m_simulationTimeStep)
            {

                Simulate();
                m_simulationTimeStamp = Time.time;

            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Simulate();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            PlaySimulation();
        }
    }

    public void PlaySimulation()
    {
        m_playSimulation = true;
    }

    public void StopSimulation()
    {
        m_playSimulation = false;
    }

    public void Simulate()
    {
        switch (m_dimensions)
        {
            case (CA_Dimension._1D):
                {
                    //REMOVE THIS NOT VALID ANYMORE
                    foreach (GameCell cell in m_allCells)
                    {
                        if (cell.m_positionOnGrid.y == 0)
                        {
                            cell.SetAlive(Random.value < 0.5, false);
                        }
                        else
                        {
                            cell.SetAlive(false, false);
                        }
                    }

                    foreach (GameCell cell in m_allCells)
                    {
                        m_calculateCellularAutomata(cell);
                    }
                    break;
                }

                case (CA_Dimension._2D):
                {
                    foreach (GameCell cell in m_allCells)
                    {
                        if (cell.UsesLastSelection())
                        {
                            m_previousCalculateCellularAutomata(cell); 
                        }
                        else
                        {
                            m_calculateCellularAutomata(cell);
                        }
                    }

                    foreach (GameCell cell in m_allCells)
                    {
                        if (cell.UsesLastSelection())
                        {
                            m_prevrioisSetCellularAutomata(cell);
                        }
                        else
                        {
                            m_setCellularAutomata(cell);
                        } 

                        cell.m_transitionSteps++;
                    }
                    break;
                }
        }

        RenderGridTexture();

        m_gridSteps++;

        Debug.Log("GRIDSTEPS - " + m_gridSteps + " CA " + SelectionManager.m_currentSelection.m_ca.ToString() + " " + SelectionManager.m_currentSelection.name);

        if (m_gridSteps == SelectionManager.m_currentSelection.m_transitionUnits)
        {
            m_gridSteps = 0;

            SelectionManager.DoTransition();
            SetupCellularAutomata();
        }
    }

    void RenderGridTexture()
    {
        int index = 0;
        for (int x = 0; x < m_rendertexture.width; x++)
        {
            for (int y = 0; y < m_rendertexture.height; y++)
            {
                Color color = m_allCells[index].GetColor();

                m_rendertexture.SetPixel(x, y, color);

                index++;
            }
        }
        m_rendertexture.Apply();

        m_renderImage.sprite = Sprite.Create(
                                                m_rendertexture,
                                                new Rect(0.0f, 0.0f, m_rendertexture.width, m_rendertexture.height),
                                                new Vector2(0.5f, 0.5f)
                                            );

        if (SelectionManager.m_captureImages)
        {
            PictureManager.s_instance.TakePicture(m_rendertexture);
        }
    }

    public int NoiseCheck(bool isAlive)
    {
        if (!isAlive) { return 0; }
        if (SelectionManager.m_currentSelection.m_useNoise)
        {
            if (Random.value < SelectionManager.m_currentSelection.m_noiseAmount)
            {
                return Random.Range(1, m_noiseFactions);
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return Random.Range(1,m_noiseFactions);
        }
    }

    static bool isInsideCircle(int circle_x, int circle_y,
                              int rad, int x, int y)
    {
        // Compare radius of circle with 
        // distance of its center from 
        // given point 
        if ((x - circle_x) * (x - circle_x) +
            (y - circle_y) * (y - circle_y) <= rad * rad)
            return true;
        else
            return false;
    }

    public void SetTransitionMarkers(int transitionTime)
    {
        foreach (GameCell cell in m_allCells)
        {
            cell.m_transitionTreshold = Random.Range(0, transitionTime);
            cell.m_transitionSteps = 0;
        }
    }
}
