﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using System.IO;
using System;

public class PictureManager : MonoBehaviour {

    public static PictureManager s_instance;

    public static int s_picCount = 0;

    void Start()
    {
        s_instance = this;
    } 
     
    public void TakePicture(Texture2D textureToRender)
    { 
        // Encode texture into PNG
        byte[] bytes = textureToRender.EncodeToPNG();

        string fileName = Application.dataPath + "/../Capture/" + String.Format("{0:D6}", s_picCount) + ".png";

        s_picCount++;

        File.WriteAllBytes(fileName, bytes); 

        Debug.Log("RenderingManager - Saved file " + fileName);
    }
}